// parameters
let p = {
  boundBox: true,
}

// the model
let handpose;
// latest model predictions
let predictions = [];
// video capture
let video;
// curtains
let curtainx = 10;
let curtain;

let counter = 0;

function preload(){
  //curtainVid = createVideo("Curtain.mp4");
  curtain = loadImage("curtain.jpg");
  view = loadImage("view.gif");
  gallery = loadImage("gallery.gif");
  forest = loadImage("forest.gif");
  nyc = loadImage("nyc.gif");
  beach = loadImage("beach.gif");
}

function setup() {
  createCanvas(640, 480)
  video = createCapture(VIDEO);
  video.size(width, height);

  // add params to a GUI
  createParamGui(p, paramChanged);

  // initialize the model
  handpose = ml5.handpose(video, modelReady);

  // This sets up an event that fills the global variable "predictions"
  // with an array every time new predictions are made
  handpose.on("predict", results => {
    predictions = results;
  });

  // Hide the video element, and just show the canvas
  video.hide();
}

function modelReady() {
  console.log("Model ready!");
}

function draw() {
  push();
  scale(-1,1);
  translate(-width, 0);
  background('#0000cc')
  //image(video, 0, 0, width, height);
  pop();
  
  let s = view;
  
  //let r = random(1,5);
  //if (r === 1){
    //s = gallery;
  //} else if (r === 2) {
      //s = view;
  //} else if (r === 3) {
      //s = forest;
  //} else if (r === 4) {
      //s = nyc;
  //} else if (r === 5) {
      //s = beach;
  //}

  image(s, 0, 0, width, height);
  
  // different visualizations
  if (p.boundBox) drawBoundingBoxes()
  curtains();

  if (predictions.length === 0){
    counter++;
  }
  if (counter === 8 ){
    curtainx = 10;
  }

}

// draw bounding boxes around all detected hands
function drawBoundingBoxes() {

  for (let p of predictions) {
    const bb = p.boundingBox
    // get bb coordinates
    const x = width - bb.bottomRight[0]
    const y = bb.topLeft[1]
    const w = bb.bottomRight[0] - bb.topLeft[0]
    const h = bb.bottomRight[1] - bb.topLeft[1]
    
    let handx = x + w/2;
    let diff = handx - curtainx;
    
    if (diff < 200) {
      curtainx = handx;
      counter = 0;
    }
  }
}

// global callback from the settings GUI
function paramChanged(name) {
}

function curtains(){
  // draw curtain
  fill("black");
  stroke("black");
  image(curtain, curtainx, 0, width, height);
}